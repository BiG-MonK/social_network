import React from 'react';
import s from './Post.module.css';
import img from './avatar.jpg';

const Post = (props) => {
    return (
        <div className={s.item}>
            <div className={s.post__wrapper}>
                <img className={s.avatar} src={img}/>
                <p className={s.text}>{props.message}</p>
            </div>
            <span>Like: {props.likesCount}</span>
        </div>
    )
}

export default Post;
import s from './DialogsItem.module.css';
import {NavLink} from 'react-router-dom';
import React from 'react';

const DialogItem = (props) => {
    let path = '/dialogs/' + props.id;
    return (
        <div className={s.dialog}>
            <NavLink className={s.dialogRef} activeClassName={s.active} to={path}>{props.name}</NavLink>
        </div>
    )
};

export default DialogItem;
import React from 'react';
import s from './Navbar.module.css'
import {NavLink} from 'react-router-dom';

const Navbar = () => {
    return (
        <nav className={s.wrapper}>
            <ul className={s.list}>
                <li className={s.item}>
                    <NavLink className={s.itemRef} to='/profile' activeClassName={s.active}>Profile</NavLink>
                </li>
                <li className={s.item}>
                    <NavLink className={s.itemRef} to='/dialogs' activeClassName={s.active}>Messages</NavLink>
                </li>
                <li className={s.item}>
                    <NavLink className={s.itemRef} to='/news' activeClassName={s.active}>News</NavLink>
                </li>
                <li className={s.item}>
                    <NavLink className={s.itemRef} to='/music' activeClassName={s.active}>Music</NavLink>
                </li>
                <li className={s.item}>
                    <NavLink className={s.itemRef} to='/settings' activeClassName={s.active}>Settings</NavLink>
                </li>
                <li className={s.item}>
                    <NavLink className={s.itemRef} to='/users' activeClassName={s.active}>Users</NavLink>
                </li>
            </ul>
        </nav>
    )
}

export default Navbar;
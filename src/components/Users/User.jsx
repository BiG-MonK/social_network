import React from 'react';
import s from "./User.module.css";
import defaultAvatar from "../../assets/defaultAvatar.jpg";
import {NavLink} from "react-router-dom";

let User = ({user, followingInProgress, unfollow, follow}) => {
    return <div className={s.userWrap}>
        <div>
            <NavLink to={'/profile/' + user.id}>
                <img className={s.userPhoto} src={user.photos.small != null
                    ? user.photos.small
                    : defaultAvatar}/>
            </NavLink>
        </div>
        <div>
            {
                user.followed
                    ? <button disabled={followingInProgress.some(id => id === user.id)}
                              onClick={() => {
                                  unfollow(user.id)
                              }}> Unfollow </button>
                    : <button disabled={followingInProgress.some(id => id === user.id)}
                              onClick={() => {
                                  follow(user.id)
                              }}> Follow </button>
            }
        </div>
        <div>
            <span>{user.name}</span>
            <span>{user.status}</span>
        </div>
        <div>
            <span>{"user.location.country"}</span>
            <br />
            <span>{"user.location.city"}</span>
        </div>
    </div>
}

export default User;
const SEND_MESSAGE = 'SEND_MESSAGE';

let initialState = {
    messages: [
        {id: 1, message: 'Hi'},
        {id: 2, message: 'How are you?'},
        {id: 3, message: 'How is your cat?'},
        {id: 4, message: 'Yo'},
        {id: 5, message: ':))'}
    ],
    dialogs: [
        {id: 1, name: 'Sergey'},
        {id: 2, name: 'Katy'},
        {id: 3, name: 'Alex'},
        {id: 4, name: 'Natasha'},
        {id: 5, name: 'Pety'}
    ]
};

export const dialogsReducer = (state = initialState, action) => {
    switch (action.type) {
        case SEND_MESSAGE:
            let newMessage = {
                id: 6,
                message: action.newMessageText
            };
            return {
                ...state,
                messages: [...state.messages, newMessage]
            };
        default:
            return state;
    }
};

export const sendMessageActionCreator = (newMessageText) => ({type: SEND_MESSAGE, newMessageText});
import React from 'react';
import s from './App.module.css'
import Navbar from './components/Navbar/Navbar';
import {Route} from 'react-router';
import News from './components/News/News';
import Music from './components/Music/Music';
import Settings from './components/Settings/Settings';
import DialogsContainer from './components/Dialogs/DialogsContainer';
import UsersContainer from './components/Users/UsersContainer';
import ProfileContainer from "./components/Profile/ProfileContainer";
import HeaderContainer from "./components/Header/HeaderContainer";
import LoginPage from "./components/Login/Login";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import {compose} from "redux";
import {initializeApp} from "./Redux/app-reducer";
import Preloader from "./components/common/Preloader/Prelader";

class App extends React.Component {
	componentDidMount() {
		this.props.initializeApp()
	}

	render() {
		if (!this.props.initialized) return <Preloader/>
		return (
			<div className={s.app}>
				<div className={s.mainWrap}>
					<HeaderContainer/>
					<Navbar/>
					<div className={s.contentWrap}>
						<Route path='/dialogs' render={() => <DialogsContainer/>}/>
						<Route path='/profile/:userId?' render={() => <ProfileContainer/>}/>
						<Route path='/users' render={() => <UsersContainer/>}/>
						<Route path='/news' render={() => <News/>}/>
						<Route path='/music' render={() => <Music/>}/>
						<Route path='/settings' render={() => <Settings/>}/>
						<Route path='/login' render={() => <LoginPage/>}/>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => ({
	initialized: state.app.initialized
})

export default compose(
	withRouter,
	connect(mapStateToProps, {initializeApp}))(App);
